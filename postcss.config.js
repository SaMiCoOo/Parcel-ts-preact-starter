const isProduction = process.env.NODE_ENV === "production";

let plugins = [
  require("tailwindcss")("./tailwind.config.js"),
  require("autoprefixer")
];
if (isProduction) {
  const purgecss = require("@fullhuman/postcss-purgecss");
  const cssnano = require("cssnano");
  plugins = plugins.concat([
    purgecss(),
    cssnano({
      preset: [
        "default",
        {
          discardComments: {
            removeAll: true
          }
        }
      ]
    })
  ]);
}
module.exports = {
  plugins
};
