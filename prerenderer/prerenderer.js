const { writeFileSync } = require("fs");
const puppeteer = require("puppeteer");
const { server, port } = require("./server");

const domain = `http://localhost:${port}`;
const pages = [
  {
    url: "/",
    htmlFile: "index.html"
  }
];

const cwd = process.cwd();
(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await Promise.all(
    pages.map(async p => {
      await page.goto(`${domain}${p.url}`, { waitUntil: "networkidle2" });
      const html = await page.content();
      writeFileSync(`${cwd}/dist/${p.htmlFile}`, html);
    })
  );
  server.end;
  await browser.close();
  server.close();
})();
