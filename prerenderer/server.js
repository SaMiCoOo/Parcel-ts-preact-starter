const express = require("express");
const app = express();

const cwd = process.cwd();
const port = 3000;

app.get("/", (_, res) => res.sendFile(`${cwd}/dist/index.html`));
app.use(express.static("dist"));

const server = app.listen(port);

module.exports = { server, port };
