import { h, Component } from "preact";

export class Clock extends Component<{}, { t: string }> {
  constructor() {
    super();
    const date = new Date();
    this.setState({ t: date.toLocaleTimeString() });
  }
  componentDidMount() {
    setInterval(() => {
      const date = new Date();
      this.setState({ t: date.toLocaleTimeString() });
    }, 1000);
  }

  render() {
    return (
      <div id="app" className="text-red-400">
        <span>{this.state.t}</span>
      </div>
    );
  }
}

export default Clock;
