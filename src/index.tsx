import { h, render } from "preact";
import App from "./components/app-component/app";

const appElement = document.getElementById("app");
const mainElement = document.getElementsByTagName("main")[0];
if (appElement) {
  render(<App />, mainElement, appElement);
} else {
  render(<App />, mainElement);
}
