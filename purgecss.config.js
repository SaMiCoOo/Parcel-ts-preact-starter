module.exports = {
  content: ["**/*.html", "**/*.tsx"],
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/\\]+/g) || []
};
